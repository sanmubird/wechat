package club.sanmubird.wechatdemo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author Sam
 */
@Controller
@RequestMapping("/index")
public class IndexController {

	@RequestMapping("/index")
	@ResponseBody
	public String test00() {
		return "访问成功！";
	}
}
